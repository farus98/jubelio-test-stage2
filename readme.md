# GETTING STARTED 

1. git clone https://farus98@bitbucket.org/farus98/jubelio-test- stage2.git 
2. cd /jubelio 
3. add file .env, the contents of the env file based on the pdf document I shared 
4. npm install 
5. npm run dev => typescript version 
   npm run start => javascript version 
6. Test via postman, based on the documentation shared above