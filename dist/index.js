"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const hapi_1 = __importDefault(require("@hapi/hapi"));
const controller_1 = require("./controller");
const init = () => __awaiter(void 0, void 0, void 0, function* () {
    const server = hapi_1.default.server({
        port: 3000,
        host: 'localhost'
    });
    server.route({
        method: 'GET',
        path: '/product/list',
        handler: (req, res) => __awaiter(void 0, void 0, void 0, function* () {
            console.log('product list ');
            const result = yield (0, controller_1.getProductList)(req.query);
            return result;
        })
    });
    server.route({
        method: 'GET',
        path: '/product/detail',
        handler: (req, res) => __awaiter(void 0, void 0, void 0, function* () {
            const result = yield (0, controller_1.getProductDetail)(req.query);
            return result;
        })
    });
    server.route({
        method: 'DELETE',
        path: '/product',
        handler: (req, res) => __awaiter(void 0, void 0, void 0, function* () {
            const result = yield (0, controller_1.deleteProduct)(req.query);
            return result;
        })
    });
    server.route({
        method: 'POST',
        path: '/product',
        handler: (req, res) => __awaiter(void 0, void 0, void 0, function* () {
            const result = yield (0, controller_1.createProduct)(req.payload);
            return result;
        })
    });
    server.route({
        method: 'PUT',
        path: '/product',
        handler: (req, res) => __awaiter(void 0, void 0, void 0, function* () {
            const result = yield (0, controller_1.updateProduct)(req.payload);
            return result;
        })
    });
    server.route({
        method: 'GET',
        path: '/product/elevenia',
        handler: (req, res) => __awaiter(void 0, void 0, void 0, function* () {
            const result = yield (0, controller_1.insertProductFromElevenia)(req.query);
            return result;
        })
    });
    server.route({
        method: 'GET',
        path: '/transaction/list',
        handler: (req, res) => __awaiter(void 0, void 0, void 0, function* () {
            const result = yield (0, controller_1.getTransactionList)(req.query);
            return result;
        })
    });
    server.route({
        method: 'GET',
        path: '/transaction/detail',
        handler: (req, res) => __awaiter(void 0, void 0, void 0, function* () {
            const result = yield (0, controller_1.getTransactionDetail)(req.query);
            return result;
        })
    });
    server.route({
        method: 'DELETE',
        path: '/transaction',
        handler: (req, res) => __awaiter(void 0, void 0, void 0, function* () {
            const result = yield (0, controller_1.deleteTransaction)(req.query);
            return result;
        })
    });
    server.route({
        method: 'POST',
        path: '/transaction',
        handler: (req, res) => __awaiter(void 0, void 0, void 0, function* () {
            const result = yield (0, controller_1.createTransaction)(req.payload);
            return result;
        })
    });
    server.route({
        method: 'PUT',
        path: '/transaction',
        handler: (req, res) => __awaiter(void 0, void 0, void 0, function* () {
            const result = yield (0, controller_1.updateTransaction)(req.payload);
            return result;
        })
    });
    server.route({
        method: 'POST',
        path: '/transaction/cancel',
        handler: (req, res) => __awaiter(void 0, void 0, void 0, function* () {
            const result = yield (0, controller_1.cancelTransaction)(req.payload);
            return result;
        })
    });
    server.route({
        method: 'POST',
        path: '/transaction/accept',
        handler: (req, res) => __awaiter(void 0, void 0, void 0, function* () {
            const result = yield (0, controller_1.acceptTransaction)(req.payload);
            return result;
        })
    });
    yield server.start();
    console.log('Server running on port 3000');
});
init();
