"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
function makeProductDb({ Query }) {
    return Object.freeze({
        showProductList,
        showProductDetail,
        deleteProductData,
        checkAvailableSku,
        checkNameProduct,
        createProduct,
        updateProduct,
        getSku,
        checkStock,
        getPriceProduct
    });
    function showProductList(body) {
        return __awaiter(this, void 0, void 0, function* () {
            return new Promise(function (resolve, reject) {
                return __awaiter(this, void 0, void 0, function* () {
                    try {
                        let limit = '';
                        let pagination = '';
                        if (body.perpage) {
                            limit += body.perpage;
                        }
                        else {
                            limit += 1000;
                        }
                        if (body.page) {
                            let offset = parseInt(body.page);
                            let page = offset - 1;
                            pagination = page * limit;
                        }
                        else {
                            pagination = 0;
                        }
                        let sql = `SELECT * FROM product ORDER BY id ASC LIMIT ${parseInt(limit)} OFFSET ${parseInt(pagination)}`;
                        let result = yield Query(sql);
                        resolve(result.rows);
                    }
                    catch (error) {
                        reject(new Error('productDb-showProductList ' + error));
                    }
                });
            });
        });
    }
    function showProductDetail(body) {
        return __awaiter(this, void 0, void 0, function* () {
            return new Promise(function (resolve, reject) {
                return __awaiter(this, void 0, void 0, function* () {
                    try {
                        let sql = `SELECT * FROM product where sku = '${body.sku}'`;
                        let result = yield Query(sql);
                        resolve(result.rows);
                    }
                    catch (error) {
                        reject(new Error('productDb-showProductDetail ' + error));
                    }
                });
            });
        });
    }
    function deleteProductData(body) {
        return __awaiter(this, void 0, void 0, function* () {
            return new Promise(function (resolve, reject) {
                return __awaiter(this, void 0, void 0, function* () {
                    try {
                        let where;
                        if (body.sku) {
                            where = "WHERE sku = '" + body.sku + "' ";
                        }
                        else {
                            where = "WHERE id = " + body.id;
                        }
                        let sql = `DELETE FROM product ${where}`;
                        let result = yield Query(sql);
                        resolve(result.rowCount);
                    }
                    catch (error) {
                        reject(new Error('productDb-deleteProductData' + error));
                    }
                });
            });
        });
    }
    function checkAvailableSku(body) {
        return __awaiter(this, void 0, void 0, function* () {
            return new Promise(function (resolve, reject) {
                return __awaiter(this, void 0, void 0, function* () {
                    try {
                        let sql = `SELECT sku FROM product where sku = '${body.getSku()}'`;
                        let result = yield Query(sql);
                        if (result.rows.length > 0) {
                            resolve(true);
                        }
                        else {
                            resolve(false);
                        }
                    }
                    catch (error) {
                        reject(new Error('productDb-checkAvailableSku ' + error));
                    }
                });
            });
        });
    }
    function checkNameProduct(body) {
        return __awaiter(this, void 0, void 0, function* () {
            return new Promise(function (resolve, reject) {
                return __awaiter(this, void 0, void 0, function* () {
                    try {
                        let sql;
                        if (body.identity == 'create') {
                            sql = `SELECT * FROM product where name = '${body.getName()}'`;
                        }
                        else {
                            sql = `SELECT * FROM product where name = '${body.getName()}' AND sku <> '${body.getSku()}'`;
                        }
                        console.log('sql', sql);
                        let result = yield Query(sql);
                        if (result.rows.length > 0) {
                            resolve(true);
                        }
                        else {
                            resolve(false);
                        }
                    }
                    catch (error) {
                        reject(new Error('productDb-checkNameProduct ' + error));
                    }
                });
            });
        });
    }
    function createProduct(body) {
        return __awaiter(this, void 0, void 0, function* () {
            return new Promise(function (resolve, reject) {
                return __awaiter(this, void 0, void 0, function* () {
                    try {
                        let sql = `INSERT INTO product (sku, name, image, price, stock, description, "createdUser", "createdTime")
                      VALUES ('${body.getSku()}', '${body.getName()}', '${body.getImage()}', ${body.getPrice()}, ${body.getStock()}, 
                      '${body.getDescription()}', '${body.getUser()}', '${body.getCreatedTime()}');`;
                        let result = yield Query(sql);
                        resolve(result.rowCount);
                    }
                    catch (error) {
                        reject(new Error('productDb-createProduct ' + error));
                    }
                });
            });
        });
    }
    function updateProduct(body) {
        return __awaiter(this, void 0, void 0, function* () {
            return new Promise(function (resolve, reject) {
                return __awaiter(this, void 0, void 0, function* () {
                    try {
                        console.log('aa', body);
                        let sql = ` UPDATE product SET 
                        name = '${body.getName()}', 
                        image = '${body.getImage()}', 
                        price = ${body.getPrice()}, 
                        stock =  ${body.getStock()}, 
                        description = '${body.getDescription()}',  
                        "updatedUser" = '${body.getUser()}', 
                        "updatedTime" = '${body.getUpdatedTime()}'
                      WHERE sku = '${body.getSku()}'`;
                        console.log('sql', sql);
                        let result = yield Query(sql);
                        resolve(result.rowCount);
                    }
                    catch (error) {
                        reject(new Error('productDb-updateProduct ' + error));
                    }
                });
            });
        });
    }
    function getSku(body) {
        return __awaiter(this, void 0, void 0, function* () {
            return new Promise(function (resolve, reject) {
                return __awaiter(this, void 0, void 0, function* () {
                    try {
                        let sql = `SELECT sku FROM product where id = '${body.id}'`;
                        let result = yield Query(sql);
                        resolve(result.rows[0].sku);
                    }
                    catch (error) {
                        reject(new Error('productDb-getSku ' + error));
                    }
                });
            });
        });
    }
    function checkStock(body) {
        return __awaiter(this, void 0, void 0, function* () {
            return new Promise(function (resolve, reject) {
                return __awaiter(this, void 0, void 0, function* () {
                    try {
                        let sql = `SELECT stock FROM product where sku = '${body.sku}'`;
                        let result = yield Query(sql);
                        resolve(result.rows[0]);
                    }
                    catch (error) {
                        reject(new Error('productDb-checkStock ' + error));
                    }
                });
            });
        });
    }
    function getPriceProduct(body) {
        return __awaiter(this, void 0, void 0, function* () {
            return new Promise(function (resolve, reject) {
                return __awaiter(this, void 0, void 0, function* () {
                    try {
                        let sql = `SELECT price FROM product where sku = '${body.sku}'`;
                        let result = yield Query(sql);
                        resolve(result.rows[0].price);
                    }
                    catch (error) {
                        reject(new Error('productDb-getPricel ' + error));
                    }
                });
            });
        });
    }
}
exports.default = makeProductDb;
