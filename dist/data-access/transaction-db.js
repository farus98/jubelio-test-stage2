"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
function makeTransactionDb({ Query }) {
    return Object.freeze({
        showTransactionList,
        showTransactionDetail,
        deleteTransactionData,
        createTransaction,
        updateTransaction,
        decreaseStock,
        increaseStock,
        getQty,
        cancelTransaction,
        acceptTransaction
    });
    function showTransactionList(body) {
        return __awaiter(this, void 0, void 0, function* () {
            return new Promise(function (resolve, reject) {
                return __awaiter(this, void 0, void 0, function* () {
                    try {
                        let limit = '';
                        let pagination = '';
                        if (body.perpage) {
                            limit += body.perpage;
                        }
                        else {
                            limit += 1000;
                        }
                        if (body.page) {
                            let offset = parseInt(body.page);
                            let page = offset - 1;
                            pagination = page * limit;
                        }
                        else {
                            pagination = 0;
                        }
                        let sql = `SELECT * FROM transaction ORDER BY id ASC LIMIT ${parseInt(limit)} OFFSET ${parseInt(pagination)}`;
                        let result = yield Query(sql);
                        resolve(result.rows);
                    }
                    catch (error) {
                        reject(new Error('transactionDb-showTransactionList ' + error));
                    }
                });
            });
        });
    }
    function showTransactionDetail(body) {
        return __awaiter(this, void 0, void 0, function* () {
            return new Promise(function (resolve, reject) {
                return __awaiter(this, void 0, void 0, function* () {
                    try {
                        let where;
                        if (body.sku) {
                            where = `WHERE sku = '${body.sku}' AND "user" = '${body.user}'`;
                        }
                        else {
                            where = "WHERE id = " + body.id;
                        }
                        let sql = `SELECT * FROM transaction ${where}`;
                        let result = yield Query(sql);
                        resolve(result.rows);
                    }
                    catch (error) {
                        reject(new Error('transactionDb-showTransactionDetail ' + error));
                    }
                });
            });
        });
    }
    function deleteTransactionData(body) {
        return __awaiter(this, void 0, void 0, function* () {
            return new Promise(function (resolve, reject) {
                return __awaiter(this, void 0, void 0, function* () {
                    try {
                        let where;
                        if (body.sku) {
                            where = "WHERE sku = '" + body.sku + "' ";
                        }
                        else {
                            where = "WHERE id = " + body.id;
                        }
                        let sql = `DELETE FROM transaction ${where}`;
                        let result = yield Query(sql);
                        resolve(result.rowCount);
                    }
                    catch (error) {
                        reject(new Error('transactionDb-deleteTransactionData' + error));
                    }
                });
            });
        });
    }
    function createTransaction(body) {
        return __awaiter(this, void 0, void 0, function* () {
            return new Promise(function (resolve, reject) {
                return __awaiter(this, void 0, void 0, function* () {
                    try {
                        let sql = `INSERT INTO transaction (sku, qty, amount, "user", "createdTime", status)
                    VALUES ('${body.getSku()}', ${body.getQty()}, ${body.getAmount()}, '${body.getUser()}', '${body.getCreatedTime()}', 'pending');`;
                        let result = yield Query(sql);
                        resolve(result.rowCount);
                    }
                    catch (error) {
                        reject(new Error('transactionDb-createTransaction ' + error));
                    }
                });
            });
        });
    }
    function updateTransaction(body) {
        return __awaiter(this, void 0, void 0, function* () {
            return new Promise(function (resolve, reject) {
                return __awaiter(this, void 0, void 0, function* () {
                    try {
                        console.log('aa', body);
                        let sql = ` UPDATE transaction SET 
                      qty = '${body.getQty()}', 
                      amount = ${body.getAmount()}, 
                      "updatedTime" = '${body.getUpdatedTime()}'
                    WHERE sku = '${body.getSku()}' AND "user" = '${body.getUser()}'`;
                        let result = yield Query(sql);
                        resolve(result.rowCount);
                    }
                    catch (error) {
                        reject(new Error('transactionDb-updateTransaction ' + error));
                    }
                });
            });
        });
    }
    function decreaseStock(body) {
        return __awaiter(this, void 0, void 0, function* () {
            return new Promise(function (resolve, reject) {
                return __awaiter(this, void 0, void 0, function* () {
                    try {
                        console.log('decrease', body);
                        let sql = ` UPDATE product SET stock = stock - ${body.qty} WHERE sku = '${body.sku}'`;
                        let result = yield Query(sql);
                        if (result.rowCount > 0) {
                            resolve("Decrease Stock Success");
                        }
                        else {
                            resolve("Decrease Stock Failed");
                        }
                    }
                    catch (error) {
                        reject(new Error('transactionDb-decreaseStock ' + error));
                    }
                });
            });
        });
    }
    function increaseStock(body) {
        return __awaiter(this, void 0, void 0, function* () {
            return new Promise(function (resolve, reject) {
                return __awaiter(this, void 0, void 0, function* () {
                    try {
                        console.log('aa', body);
                        let sql = ` UPDATE product SET stock = stock + ${body.qty} WHERE sku = '${body.sku}'`;
                        let result = yield Query(sql);
                        if (result.rowCount > 0) {
                            resolve("Decrease Stock Success");
                        }
                        else {
                            resolve("Decrease Stock Failed");
                        }
                    }
                    catch (error) {
                        reject(new Error('transactionDb-increaseStock ' + error));
                    }
                });
            });
        });
    }
    function getQty(body) {
        return __awaiter(this, void 0, void 0, function* () {
            return new Promise(function (resolve, reject) {
                return __awaiter(this, void 0, void 0, function* () {
                    try {
                        // console.log('aa', body);
                        let sql = `SELECT qty FROM transaction WHERE sku = '${body.sku}' AND "user" = '${body.user}'`;
                        console.log('sql', sql);
                        let result = yield Query(sql);
                        resolve(result.rows[0].qty);
                    }
                    catch (error) {
                        reject(new Error('transactionDb-getQty ' + error));
                    }
                });
            });
        });
    }
    function cancelTransaction(body) {
        return __awaiter(this, void 0, void 0, function* () {
            return new Promise(function (resolve, reject) {
                return __awaiter(this, void 0, void 0, function* () {
                    try {
                        let sql = ` UPDATE transaction SET status = 'canceled', "updatedTime" = '${body.getUpdatedTime()}'
                      WHERE sku = '${body.getSku()}' AND "user" = '${body.getUser()}'`;
                        let result = yield Query(sql);
                        resolve(result.rowCount);
                    }
                    catch (error) {
                        reject(new Error('transactionDb-cancelTransaction' + error));
                    }
                });
            });
        });
    }
    function acceptTransaction(body) {
        return __awaiter(this, void 0, void 0, function* () {
            return new Promise(function (resolve, reject) {
                return __awaiter(this, void 0, void 0, function* () {
                    try {
                        let sql = ` UPDATE transaction SET status = 'accepted', "updatedTime" = '${body.getUpdatedTime()}'
                      WHERE sku = '${body.getSku()}' AND "user" = '${body.getUser()}'`;
                        let result = yield Query(sql);
                        resolve(result.rowCount);
                    }
                    catch (error) {
                        reject(new Error('transactionDb-acceptTransaction' + error));
                    }
                });
            });
        });
    }
}
exports.default = makeTransactionDb;
