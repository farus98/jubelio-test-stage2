"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.transactionDb = exports.productDb = void 0;
const { Pool } = require('pg');
require('dotenv').config();
const product_db_1 = __importDefault(require("./product-db"));
const transaction_db_1 = __importDefault(require("./transaction-db"));
let host = process.env.PGHOST;
let user = process.env.PGUSER;
let password = process.env.PGPASWORD;
let database = process.env.PGDATABASE;
let port = process.env.PGPORT;
const pool = new Pool({
    user: user,
    host: host,
    database: database,
    password: password,
    port: port,
});
pool.connect((err) => {
    if (!err)
        console.log('DB koneksi master suksess');
    else
        console.log('DB koneksi master error : ' + err);
});
function Query(sintax) {
    return new Promise((resolve, reject) => __awaiter(this, void 0, void 0, function* () {
        pool.query(sintax, (err, res) => {
            if (err) {
                reject(new Error("querry error " + err));
            }
            else {
                resolve(res);
            }
            // console.log('user:', res.rows[0])
        });
    }));
}
const productDb = (0, product_db_1.default)({ Query });
exports.productDb = productDb;
const transactionDb = (0, transaction_db_1.default)({ Query });
exports.transactionDb = transactionDb;
const jubelioModel = Object.freeze({
    productDb,
    transactionDb
});
exports.default = (jubelioModel);
