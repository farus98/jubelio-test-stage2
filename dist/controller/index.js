"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.acceptTransaction = exports.cancelTransaction = exports.getTransactionDetail = exports.getTransactionList = exports.deleteTransaction = exports.updateTransaction = exports.createTransaction = exports.insertProductFromElevenia = exports.updateProduct = exports.createProduct = exports.deleteProduct = exports.getProductDetail = exports.getProductList = void 0;
const use_case_1 = require("../use-case");
const get_product_list_1 = __importDefault(require("./product/get-product-list"));
const get_product_detail_1 = __importDefault(require("./product/get-product-detail"));
const delete_product_1 = __importDefault(require("./product/delete-product"));
const create_product_1 = __importDefault(require("./product/create-product"));
const update_product_1 = __importDefault(require("./product/update-product"));
const insert_product_from_elevenia_1 = __importDefault(require("./product/insert-product-from-elevenia"));
const create_transaction_1 = __importDefault(require("./transaction/create-transaction"));
const update_transaction_1 = __importDefault(require("./transaction/update-transaction"));
const delete_transaction_1 = __importDefault(require("./transaction/delete-transaction"));
const get_transaction_1 = __importDefault(require("./transaction/get-transaction"));
const get_transaction_detail_1 = __importDefault(require("./transaction/get-transaction-detail"));
const cancel_transaction_1 = __importDefault(require("./transaction/cancel-transaction"));
const accept_transaction_1 = __importDefault(require("./transaction/accept-transaction"));
const getProductList = (0, get_product_list_1.default)({ getDataProductList: use_case_1.getDataProductList });
exports.getProductList = getProductList;
const getProductDetail = (0, get_product_detail_1.default)({ getDataProductDetail: use_case_1.getDataProductDetail });
exports.getProductDetail = getProductDetail;
const deleteProduct = (0, delete_product_1.default)({ deleteDataProduct: use_case_1.deleteDataProduct });
exports.deleteProduct = deleteProduct;
const createProduct = (0, create_product_1.default)({ createDataProduct: use_case_1.createDataProduct });
exports.createProduct = createProduct;
const updateProduct = (0, update_product_1.default)({ updateDataProduct: use_case_1.updateDataProduct });
exports.updateProduct = updateProduct;
const insertProductFromElevenia = (0, insert_product_from_elevenia_1.default)({ insertDataProductFromElevenia: use_case_1.insertDataProductFromElevenia });
exports.insertProductFromElevenia = insertProductFromElevenia;
const createTransaction = (0, create_transaction_1.default)({ createDataTransaction: use_case_1.createDataTransaction });
exports.createTransaction = createTransaction;
const updateTransaction = (0, update_transaction_1.default)({ updateDataTransaction: use_case_1.updateDataTransaction });
exports.updateTransaction = updateTransaction;
const deleteTransaction = (0, delete_transaction_1.default)({ deleteDataTransaction: use_case_1.deleteDataTransaction });
exports.deleteTransaction = deleteTransaction;
const getTransactionList = (0, get_transaction_1.default)({ getDataTransactionList: use_case_1.getDataTransactionList });
exports.getTransactionList = getTransactionList;
const getTransactionDetail = (0, get_transaction_detail_1.default)({ getDataTransactionDetail: use_case_1.getDataTransactionDetail });
exports.getTransactionDetail = getTransactionDetail;
const cancelTransaction = (0, cancel_transaction_1.default)({ cancelDataTransaction: use_case_1.cancelDataTransaction });
exports.cancelTransaction = cancelTransaction;
const acceptTransaction = (0, accept_transaction_1.default)({ acceptDataTransaction: use_case_1.acceptDataTransaction });
exports.acceptTransaction = acceptTransaction;
const ecommerceController = Object.freeze({
    getProductList,
    getProductDetail,
    deleteProduct,
    createProduct,
    updateProduct,
    insertProductFromElevenia,
    createTransaction,
    updateTransaction,
    deleteTransaction,
    getTransactionList,
    getTransactionDetail,
    cancelTransaction,
    acceptTransaction
});
exports.default = ecommerceController;
