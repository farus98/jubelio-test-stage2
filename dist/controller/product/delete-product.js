"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
function makeDeleteProduct({ deleteDataProduct }) {
    return function deleteProduct(httpRequest) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const posted = yield deleteDataProduct(httpRequest);
                return Object.assign({ status: true }, posted);
            }
            catch (err) {
                //TODO: Error logging
                console.log(err);
                return {
                    statusCode: 404,
                    body: {
                        status: false,
                        responseCode: 404,
                        message: err.message
                    }
                };
            }
        });
    };
}
exports.default = makeDeleteProduct;
