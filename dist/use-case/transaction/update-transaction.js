"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
function makeUpdateTransaction({ transactionDb, productDb, makeTransaction }) {
    return function updateTransaction(body) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                console.log('usecase update transaction');
                if (body.qty <= 0) {
                    throw new Error("Qty Must Be More Than 0");
                }
                const getQty = yield transactionDb.getQty(body);
                console.log('getQty', getQty);
                const getPrice = yield productDb.getPriceProduct({ sku: body.sku });
                const totalPrice = getPrice * body.qty;
                const transaction = yield makeTransaction(Object.assign(Object.assign({}, body), { amount: totalPrice }));
                const updateTransaction = yield transactionDb.updateTransaction(transaction);
                let result;
                if (updateTransaction > 0) {
                    if (transaction.getQty() < getQty) {
                        //increase
                        const increaseStock = yield transactionDb.increaseStock({ sku: transaction.getSku(), qty: transaction.getQty() });
                        console.log('increase stock', increaseStock);
                    }
                    else {
                        //decrease
                        const decreaseStock = yield transactionDb.decreaseStock({ sku: transaction.getSku(), qty: transaction.getQty() });
                        console.log('decrease stock', decreaseStock);
                    }
                    result = { responseCode: 201, information: "Your Transaction Has Been Successfully Updated" };
                }
                else {
                    result = { responseCode: 400, information: "Your Transaction Failed Updated" };
                }
                return result;
            }
            catch (error) {
                throw new Error(error);
            }
        });
    };
}
exports.default = makeUpdateTransaction;
