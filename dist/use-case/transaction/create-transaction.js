"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
function makeCreateTransaction({ transactionDb, productDb, makeTransaction }) {
    return function createTransaction(body) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const checkStock = yield productDb.checkStock({ sku: body.sku });
                let result;
                if (checkStock.stock > 0) {
                    if (body.qty > checkStock.stock) {
                        throw new Error("Transaction Failed Due To Insufficient Stock Quantity");
                    }
                    const getPrice = yield productDb.getPriceProduct({ sku: body.sku });
                    const totalPrice = getPrice * body.qty;
                    const transaction = yield makeTransaction(Object.assign(Object.assign({}, body), { amount: totalPrice }));
                    const insertTransaction = yield transactionDb.createTransaction(transaction);
                    if (insertTransaction > 0) {
                        const decreaseStock = yield transactionDb.decreaseStock({ sku: transaction.getSku(), qty: transaction.getQty() });
                        console.log('decrease stock', decreaseStock);
                        result = { responseCode: 201, information: "Your Transaction is Successfully Recorded, Your Purchase Amount is " + totalPrice };
                    }
                    else {
                        result = { responseCode: 400, information: "Your Transaction Failed Recorded" };
                    }
                }
                else {
                    throw new Error("Stock Product Less Then 0 Can't Make Transactions");
                }
                return result;
            }
            catch (error) {
                throw new Error(error);
            }
        });
    };
}
exports.default = makeCreateTransaction;
