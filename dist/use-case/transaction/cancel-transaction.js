"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
function makeCancelTransaction({ transactionDb, makeTransaction }) {
    return function cancelTransaction(body) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const order = yield makeTransaction(body);
                const cancelOrder = yield transactionDb.cancelOrder(order);
                let result;
                if (cancelOrder > 0) {
                    result = { responseCode: 201, information: "order with sku " + body.sku + " and user " + body.user + " has been canceled " };
                }
                else {
                    result = { responseCode: 400, information: "order with sku " + body.sku + " and user " + body.user + " failed to canceled " };
                }
                return result;
            }
            catch (error) {
                throw new Error(error);
            }
        });
    };
}
exports.default = makeCancelTransaction;
