"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.acceptDataTransaction = exports.cancelDataTransaction = exports.deleteDataTransaction = exports.getDataTransactionDetail = exports.getDataTransactionList = exports.updateDataTransaction = exports.createDataTransaction = exports.insertDataProductFromElevenia = exports.updateDataProduct = exports.createDataProduct = exports.deleteDataProduct = exports.getDataProductDetail = exports.getDataProductList = void 0;
const data_access_1 = require("../data-access");
const product_1 = __importDefault(require("../entity/product"));
const transaction_1 = __importDefault(require("../entity/transaction"));
const middleware_1 = require("../middleware");
const get_product_list_1 = __importDefault(require("./product/get-product-list"));
const get_product_detail_1 = __importDefault(require("./product/get-product-detail"));
const delete_product_1 = __importDefault(require("./product/delete-product"));
const create_product_1 = __importDefault(require("./product/create-product"));
const update_product_1 = __importDefault(require("./product/update-product"));
const insert_product_from_elevenia_1 = __importDefault(require("./product/insert-product-from-elevenia"));
const create_transaction_1 = __importDefault(require("./transaction/create-transaction"));
const update_transaction_1 = __importDefault(require("./transaction/update-transaction"));
const get_transaction_list_1 = __importDefault(require("./transaction/get-transaction-list"));
const get_transaction_detail_1 = __importDefault(require("./transaction/get-transaction-detail"));
const delete_transaction_1 = __importDefault(require("./transaction/delete-transaction"));
const cancel_transaction_1 = __importDefault(require("./transaction/cancel-transaction"));
const accept_transaction_1 = __importDefault(require("./transaction/accept-transaction"));
const getDataProductList = (0, get_product_list_1.default)({ productDb: data_access_1.productDb });
exports.getDataProductList = getDataProductList;
const getDataProductDetail = (0, get_product_detail_1.default)({ productDb: data_access_1.productDb });
exports.getDataProductDetail = getDataProductDetail;
const deleteDataProduct = (0, delete_product_1.default)({ productDb: data_access_1.productDb, transactionDb: data_access_1.transactionDb });
exports.deleteDataProduct = deleteDataProduct;
const createDataProduct = (0, create_product_1.default)({ productDb: data_access_1.productDb, makeProduct: product_1.default });
exports.createDataProduct = createDataProduct;
const updateDataProduct = (0, update_product_1.default)({ productDb: data_access_1.productDb, makeProduct: product_1.default });
exports.updateDataProduct = updateDataProduct;
const insertDataProductFromElevenia = (0, insert_product_from_elevenia_1.default)({ productDb: data_access_1.productDb, makeProduct: product_1.default, requestElevenia: middleware_1.requestElevenia });
exports.insertDataProductFromElevenia = insertDataProductFromElevenia;
const createDataTransaction = (0, create_transaction_1.default)({ transactionDb: data_access_1.transactionDb, productDb: data_access_1.productDb, makeTransaction: transaction_1.default });
exports.createDataTransaction = createDataTransaction;
const updateDataTransaction = (0, update_transaction_1.default)({ transactionDb: data_access_1.transactionDb, productDb: data_access_1.productDb, makeTransaction: transaction_1.default });
exports.updateDataTransaction = updateDataTransaction;
const getDataTransactionList = (0, get_transaction_list_1.default)({ transactionDb: data_access_1.transactionDb });
exports.getDataTransactionList = getDataTransactionList;
const getDataTransactionDetail = (0, get_transaction_detail_1.default)({ transactionDb: data_access_1.transactionDb });
exports.getDataTransactionDetail = getDataTransactionDetail;
const deleteDataTransaction = (0, delete_transaction_1.default)({ transactionDb: data_access_1.transactionDb });
exports.deleteDataTransaction = deleteDataTransaction;
const cancelDataTransaction = (0, cancel_transaction_1.default)({ transactionDb: data_access_1.transactionDb, makeTransaction: transaction_1.default });
exports.cancelDataTransaction = cancelDataTransaction;
const acceptDataTransaction = (0, accept_transaction_1.default)({ transactionDb: data_access_1.transactionDb, makeTransaction: transaction_1.default });
exports.acceptDataTransaction = acceptDataTransaction;
const ecommerceService = Object.freeze({
    getDataProductList,
    getDataProductDetail,
    deleteDataProduct,
    createDataProduct,
    updateDataProduct,
    insertDataProductFromElevenia,
    createDataTransaction,
    updateDataTransaction,
    getDataTransactionList,
    getDataTransactionDetail,
    deleteDataTransaction,
    cancelDataTransaction,
    acceptDataTransaction
});
exports.default = ecommerceService;
