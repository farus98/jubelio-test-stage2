"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
function makeGetProductDetail({ productDb }) {
    return function getProductDetail(body) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const getData = yield productDb.showProductDetail(body);
                // console.log('data', getData);
                let result;
                if (getData.length > 0) {
                    result = { responseCode: 200, information: "Data Found", data: getData[0] };
                }
                else {
                    result = { responseCode: 204, information: "Data Not Found", data: {} };
                }
                return Object.freeze(result);
            }
            catch (error) {
                throw new Error(error);
            }
        });
    };
}
exports.default = makeGetProductDetail;
