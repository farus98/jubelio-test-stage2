"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const xml2js = require('xml2js');
function makeInsertProductFromElevenia({ productDb, makeProduct, requestElevenia }) {
    return function insertProductFromElevenia(body) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const getDataElevenia = yield requestElevenia.getDataProductElevenia();
                let jsonDataElevenia;
                xml2js.parseString(getDataElevenia, (err, result) => {
                    if (err) {
                        throw err;
                    }
                    jsonDataElevenia = result.Products.product;
                });
                let insertData = yield Promise.all(jsonDataElevenia.map((data) => __awaiter(this, void 0, void 0, function* () {
                    const entity = {
                        sku: data.sellerPrdCd[0],
                        name: data.prdNm[0],
                        image: data.imageKindChk[0],
                        price: data.selPrc[0],
                        stock: data.ProductOptionDetails[0].stckQty[0],
                        description: '',
                        user: 'system',
                    };
                    const product = yield makeProduct(entity);
                    const checkSkuAvaliable = yield productDb.checkAvailableSku(product);
                    if (checkSkuAvaliable == true) {
                        return { responseCode: 400, information: "Product SKU " + entity.sku + " already exists, cannot be duplicated" };
                    }
                    else {
                        const checkAvailableNameProduct = productDb.checkNameProduct(Object.assign(Object.assign({}, product), { identity: 'create' }));
                        if (checkAvailableNameProduct == true) {
                            return { responseCode: 400, information: "Product Name with SKU " + entity.sku + " already exists, cannot be duplicated" };
                        }
                        else {
                            const insertData = yield productDb.createProduct(product);
                            if (insertData > 0) {
                                return { information: "Data SKU " + product.getSku() + " Success Inserted" };
                            }
                            else {
                                return { information: "Data SKU " + product.getSku() + " Failed Inserted" };
                            }
                        }
                    }
                })));
                return insertData;
            }
            catch (error) {
                throw new Error(error);
            }
        });
    };
}
exports.default = makeInsertProductFromElevenia;
