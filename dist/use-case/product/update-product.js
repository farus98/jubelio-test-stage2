"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
function makeUpdateProduct({ productDb, makeProduct }) {
    return function updateProduct(body) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const product = yield makeProduct(body);
                const checkAvailableNameProduct = yield productDb.checkNameProduct(Object.assign(Object.assign({}, product), { identity: 'update' }));
                console.log('check name', checkAvailableNameProduct);
                if (checkAvailableNameProduct == true) {
                    throw new Error("Name already exists, cannot be duplicated");
                }
                const updateData = yield productDb.updateProduct(product);
                let result;
                if (updateData > 0) {
                    result = { responseCode: 201, information: "Data SKU " + product.getSku() + " Success Updated" };
                }
                else {
                    result = { responseCode: 400, information: "Data SKU " + product.getSku() + " Failed Updated" };
                }
                return result;
            }
            catch (error) {
                throw new Error(error);
            }
        });
    };
}
exports.default = makeUpdateProduct;
