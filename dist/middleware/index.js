"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.requestElevenia = void 0;
const elevenia_1 = __importDefault(require("./elevenia"));
const requestElevenia = (0, elevenia_1.default)();
exports.requestElevenia = requestElevenia;
const jubelioMiddleware = Object.freeze({
    requestElevenia
});
exports.default = jubelioMiddleware;
