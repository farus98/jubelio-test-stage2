"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const axios_1 = __importDefault(require("axios"));
function makeRequestElevenia() {
    return Object.freeze({
        getDataProductElevenia
    });
    function getDataProductElevenia(reqsms) {
        return __awaiter(this, void 0, void 0, function* () {
            return new Promise(function (resolve, reject) {
                (0, axios_1.default)({
                    method: 'GET',
                    url: "http://api.elevenia.co.id/rest/prodservices/product/listing?page=1",
                    headers: {
                        'Content-Type': 'application/xml',
                        openapikey: '721407f393e84a28593374cc2b347a98'
                    }
                })
                    .then(result => {
                    // console.log('data',result)
                    resolve(result.data);
                })
                    .catch(err => {
                    // console.log('err',err)
                    reject(new Error(err));
                });
            });
        });
    }
}
exports.default = makeRequestElevenia;
