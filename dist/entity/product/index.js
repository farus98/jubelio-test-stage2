"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const product_1 = __importDefault(require("./product"));
const moment_1 = __importDefault(require("moment"));
moment_1.default.locale('id');
const makeProduct = (0, product_1.default)(moment_1.default);
exports.default = makeProduct;
