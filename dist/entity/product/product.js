"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
function buildMakeProduct(moment) {
    return function makeProduct({ sku = '', name = '', image = '', price = '', stock = '', description = '', user = '', createdTime = moment().format('YYYY-MM-DD HH:mm:ss'), updateTime = moment().format('YYYY-MM-DD HH:mm:ss') } = {}) {
        return Object.freeze({
            getSku: () => sku,
            getName: () => name,
            getImage: () => image,
            getPrice: () => price,
            getStock: () => stock,
            getDescription: () => description,
            getUser: () => user,
            getCreatedTime: () => createdTime,
            getUpdatedTime: () => updateTime
        });
    };
}
exports.default = buildMakeProduct;
