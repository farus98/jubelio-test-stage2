"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
function buildMakeTransaction(moment) {
    return function makeTransaction({ sku = '', qty = 0, amount = 0, user = '', createdTime = moment().format('YYYY-MM-DD HH:mm:ss'), updateTime = moment().format('YYYY-MM-DD HH:mm:ss') } = {}) {
        return Object.freeze({
            getSku: () => sku,
            getQty: () => qty,
            getAmount: () => amount,
            getUser: () => user,
            getCreatedTime: () => createdTime,
            getUpdatedTime: () => updateTime
        });
    };
}
exports.default = buildMakeTransaction;
