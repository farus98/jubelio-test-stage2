"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const transaction_1 = __importDefault(require("./transaction"));
const moment_1 = __importDefault(require("moment"));
moment_1.default.locale('id');
const makeTransaction = (0, transaction_1.default)(moment_1.default);
exports.default = makeTransaction;
