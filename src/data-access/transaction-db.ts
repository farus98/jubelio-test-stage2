export default function makeTransactionDb({Query}) {
  return Object.freeze({
    showTransactionList, 
    showTransactionDetail,
    deleteTransactionData,
    createTransaction,
    updateTransaction, 
    decreaseStock, 
    increaseStock, 
    getQty, 
    cancelTransaction, 
    acceptTransaction
  })

  async function showTransactionList(body){
    return new Promise(async function(resolve, reject) {
      try{
      
        let limit:any = ''
        let pagination:any = ''
        
        if(body.perpage){
          limit += body.perpage ;
        }else{
          limit += 1000
        }
        
        if(body.page){
          let offset = parseInt(body.page);
          let page = offset - 1;
          pagination = page*limit;
        }else{
          pagination = 0;
        }

        let sql = `SELECT * FROM transaction ORDER BY id ASC LIMIT ${parseInt(limit)} OFFSET ${parseInt(pagination)}`;
      
        let result = await Query(sql);

        resolve(result.rows)
      
      } catch(error){
        reject(new Error('transactionDb-showTransactionList '+error));
      }
    })
  }

  async function showTransactionDetail(body){
    return new Promise(async function(resolve, reject) {
      try{
        let where

        if(body.sku){
          where = `WHERE sku = '${body.sku}' AND "user" = '${body.user}'`
        }else{
          where = "WHERE id = "+body.id
        }
        
        let sql = `SELECT * FROM transaction ${where}`;
        
        let result = await Query(sql);

        resolve(result.rows)
      
      } catch(error){
        reject(new Error('transactionDb-showTransactionDetail '+error));
      }
    })
  }

  async function deleteTransactionData(body){
    return new Promise(async function(resolve, reject) {
      try{

        let where:any

        if(body.sku){
          where = "WHERE sku = '"+body.sku+"' ";
        }else{
          where = "WHERE id = "+body.id
        } 

        let sql = `DELETE FROM transaction ${where}`;
        
        let result = await Query(sql);
        
        resolve(result.rowCount)
      
      } catch(error){
        reject(new Error('transactionDb-deleteTransactionData'+error));
      }
    })
  }

  async function createTransaction(body){
    return new Promise(async function(resolve, reject) {
      try{
        
        let sql = `INSERT INTO transaction (sku, qty, amount, "user", "createdTime", status)
                    VALUES ('${body.getSku()}', ${body.getQty()}, ${body.getAmount()}, '${body.getUser()}', '${body.getCreatedTime()}', 'pending');`;
        
        let result = await Query(sql);

        resolve(result.rowCount)
      
      } catch(error){
        reject(new Error('transactionDb-createTransaction '+error));
      }
    })
  }

  async function updateTransaction(body){
    return new Promise(async function(resolve, reject) {
      try{
      console.log('aa', body);
      
        let sql = ` UPDATE transaction SET 
                      qty = '${body.getQty()}', 
                      amount = ${body.getAmount()}, 
                      "updatedTime" = '${body.getUpdatedTime()}'
                    WHERE sku = '${body.getSku()}' AND "user" = '${body.getUser()}'`;
        
        let result = await Query(sql);
        
        resolve(result.rowCount)
      
      } catch(error){
        reject(new Error('transactionDb-updateTransaction '+error));
      }
    })
  }

  async function decreaseStock(body){
    return new Promise(async function(resolve, reject) {
      try{
      console.log('decrease', body);
      
        let sql = ` UPDATE product SET stock = stock - ${body.qty} WHERE sku = '${body.sku}'`;

        let result = await Query(sql);
        
        if(result.rowCount > 0){
          resolve("Decrease Stock Success")
        }else{
          resolve("Decrease Stock Failed")
        }
      
      } catch(error){
        reject(new Error('transactionDb-decreaseStock '+error));
      }
    })
  }

  async function increaseStock(body){
    return new Promise(async function(resolve, reject) {
      try{
      console.log('aa', body);
      
        let sql = ` UPDATE product SET stock = stock + ${body.qty} WHERE sku = '${body.sku}'`;

        let result = await Query(sql);
        
        if(result.rowCount > 0){
          resolve("Decrease Stock Success")
        }else{
          resolve("Decrease Stock Failed")
        }
      
      } catch(error){
        reject(new Error('transactionDb-increaseStock '+error));
      }
    })
  }

  async function getQty(body){
    return new Promise(async function(resolve, reject) {
      try{
      // console.log('aa', body);
      
        let sql = `SELECT qty FROM transaction WHERE sku = '${body.sku}' AND "user" = '${body.user}'`;
        console.log('sql', sql);
        
        let result = await Query(sql);
        
        resolve(result.rows[0].qty)
      
      } catch(error){
        reject(new Error('transactionDb-getQty '+error));
      }
    })
  }

  async function cancelTransaction(body){
    return new Promise(async function(resolve, reject) {
      try{

        let sql = ` UPDATE transaction SET status = 'canceled', "updatedTime" = '${body.getUpdatedTime()}'
                      WHERE sku = '${body.getSku()}' AND "user" = '${body.getUser()}'`;
        
        let result = await Query(sql);
        
        resolve(result.rowCount)
      
      } catch(error){
        reject(new Error('transactionDb-cancelTransaction'+error));
      }
    })
  }

  async function acceptTransaction(body){
    return new Promise(async function(resolve, reject) {
      try{

        let sql = ` UPDATE transaction SET status = 'accepted', "updatedTime" = '${body.getUpdatedTime()}'
                      WHERE sku = '${body.getSku()}' AND "user" = '${body.getUser()}'`;
        
        let result = await Query(sql);
        
        resolve(result.rowCount)
      
      } catch(error){
        reject(new Error('transactionDb-acceptTransaction'+error));
      }
    })
  }

}