export default function makeProductDb({Query}) {
    return Object.freeze({
      showProductList, 
      showProductDetail,
      deleteProductData,
      checkAvailableSku,
      checkNameProduct,
      createProduct,
      updateProduct, 
      getSku, 
      checkStock, 
      getPriceProduct
    })

    async function showProductList(body){
      return new Promise(async function(resolve, reject) {
        try{
        
          let limit:any = ''
          let pagination:any = ''
          
          if(body.perpage){
            limit += body.perpage ;
          }else{
            limit += 1000
          }
          
          if(body.page){
            let offset = parseInt(body.page);
            let page = offset - 1;
            pagination = page*limit;
          }else{
            pagination = 0;
          }

          let sql = `SELECT * FROM product ORDER BY id ASC LIMIT ${parseInt(limit)} OFFSET ${parseInt(pagination)}`;
        
          let result = await Query(sql);
  
          resolve(result.rows)
        
        } catch(error){
          reject(new Error('productDb-showProductList '+error));
        }
      })
    }

    async function showProductDetail(body){
      return new Promise(async function(resolve, reject) {
        try{
          
          let sql = `SELECT * FROM product where sku = '${body.sku}'`;
          
          let result = await Query(sql);
  
          resolve(result.rows)
        
        } catch(error){
          reject(new Error('productDb-showProductDetail '+error));
        }
      })
    }

    async function deleteProductData(body){
      return new Promise(async function(resolve, reject) {
        try{

          let where:any

          if(body.sku){
            where = "WHERE sku = '"+body.sku+"' ";
          }else{
            where = "WHERE id = "+body.id
          } 

          let sql = `DELETE FROM product ${where}`;
          
          let result = await Query(sql);
          
          resolve(result.rowCount)
        
        } catch(error){
          reject(new Error('productDb-deleteProductData'+error));
        }
      })
    }

    async function checkAvailableSku(body){
      return new Promise(async function(resolve, reject) {
        try{
        
          let  sql = `SELECT sku FROM product where sku = '${body.getSku()}'`; 
 
          let result = await Query(sql);
          
          if(result.rows.length > 0){
            resolve(true)
          }else{
            resolve(false)
          }
        
        } catch(error){
          reject(new Error('productDb-checkAvailableSku '+error));
        }
      })
    }

    async function checkNameProduct(body){
      return new Promise(async function(resolve, reject) {
        try{
        
          let sql 

          if(body.identity == 'create'){
            sql = `SELECT * FROM product where name = '${body.getName()}'`;
          }else{
            sql = `SELECT * FROM product where name = '${body.getName()}' AND sku <> '${body.getSku()}'`;
          }
          console.log('sql', sql);
          
          let result = await Query(sql);
          
          if(result.rows.length > 0){
            resolve(true)
          }else{
            resolve(false)
          }
        
        } catch(error){
          reject(new Error('productDb-checkNameProduct '+error));
        }
      })
    }

    async function createProduct(body){
      return new Promise(async function(resolve, reject) {
        try{
        
          let sql = `INSERT INTO product (sku, name, image, price, stock, description, "createdUser", "createdTime")
                      VALUES ('${body.getSku()}', '${body.getName()}', '${body.getImage()}', ${body.getPrice()}, ${body.getStock()}, 
                      '${body.getDescription()}', '${body.getUser()}', '${body.getCreatedTime()}');`;
          
          let result = await Query(sql);

          resolve(result.rowCount)
        
        } catch(error){
          reject(new Error('productDb-createProduct '+error));
        }
      })
    }

    async function updateProduct(body){
      return new Promise(async function(resolve, reject) {
        try{
        console.log('aa', body);
        
          let sql = ` UPDATE product SET 
                        name = '${body.getName()}', 
                        image = '${body.getImage()}', 
                        price = ${body.getPrice()}, 
                        stock =  ${body.getStock()}, 
                        description = '${body.getDescription()}',  
                        "updatedUser" = '${body.getUser()}', 
                        "updatedTime" = '${body.getUpdatedTime()}'
                      WHERE sku = '${body.getSku()}'`;
          console.log('sql',sql);
          
          let result = await Query(sql);
          
          resolve(result.rowCount)
        
        } catch(error){
          reject(new Error('productDb-updateProduct '+error));
        }
      })
    }

    async function getSku(body){
      return new Promise(async function(resolve, reject) {
        try{
          
          let sql = `SELECT sku FROM product where id = '${body.id}'`;
          
          let result = await Query(sql);
  
          resolve(result.rows[0].sku)
        
        } catch(error){
          reject(new Error('productDb-getSku '+error));
        }
      })
    }
  
    async function checkStock(body){
      return new Promise(async function(resolve, reject) {
        try{
          
          let sql = `SELECT stock FROM product where sku = '${body.sku}'`;
          
          let result = await Query(sql);
  
          resolve(result.rows[0])
        
        } catch(error){
          reject(new Error('productDb-checkStock '+error));
        }
      })
    }

    async function getPriceProduct(body){
      return new Promise(async function(resolve, reject) {
        try{
          
          let sql = `SELECT price FROM product where sku = '${body.sku}'`;
          
          let result = await Query(sql);
  
          resolve(result.rows[0].price)
        
        } catch(error){
          reject(new Error('productDb-getPricel '+error));
        }
      })
    }

}
  