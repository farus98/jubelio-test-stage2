const { Pool } = require('pg')
require('dotenv').config();

import makeProductDb from './product-db'
import makeTransactionDb from './transaction-db'

let host:any = process.env.PGHOST
let user:any = process.env.PGUSER
let password:any = process.env.PGPASWORD
let database:any = process.env.PGDATABASE
let port:any = process.env.PGPORT

const pool = new Pool({
  user: user,
  host: host,
  database: database,
  password: password,
  port: port,
})

pool.connect((err:any) => {
  if (!err)
  console.log('DB koneksi master suksess');

  else
  console.log('DB koneksi master error : ' + err);
})

function Query(sintax:any) {
  return new Promise(async (resolve, reject) => {

    pool.query(sintax, (err: any, res: { rows: any[]; }) => {
      if (err) {
        reject(new Error("querry error " + err));
      }else{
        resolve(res);
      }
      // console.log('user:', res.rows[0])
    })

  });
}

const productDb = makeProductDb({Query})
const transactionDb = makeTransactionDb({Query})

const jubelioModel = Object.freeze({
  productDb, 
  transactionDb
})

export default (jubelioModel)
export {
  productDb, 
  transactionDb
}
