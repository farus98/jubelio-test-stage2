import {
    getDataProductList,
    getDataProductDetail,
    deleteDataProduct,
    createDataProduct,
    updateDataProduct,
    insertDataProductFromElevenia, 

    createDataTransaction, 
    updateDataTransaction, 
    getDataTransactionList, 
    getDataTransactionDetail, 
    deleteDataTransaction, 
    cancelDataTransaction, 
    acceptDataTransaction
} from "../use-case"

import makeGetProductList from "./product/get-product-list"
import makeGetProductDetail from "./product/get-product-detail"
import makeDeleteProduct from "./product/delete-product"
import makeCreateProduct from "./product/create-product"
import makeUpdateProduct from "./product/update-product"
import makeInsertProductFromElevenia from "./product/insert-product-from-elevenia"

import makeCreateTransaction from "./transaction/create-transaction"
import makeUpdateTransaction from "./transaction/update-transaction"
import makeDeleteTransaction from "./transaction/delete-transaction"
import makeGetTransactionList from "./transaction/get-transaction"
import makeGetTransactionDetail from "./transaction/get-transaction-detail"
import makeCancelTransaction from "./transaction/cancel-transaction"
import makeAcceptTransaction from "./transaction/accept-transaction"

const getProductList = makeGetProductList({getDataProductList})
const getProductDetail = makeGetProductDetail({getDataProductDetail})
const deleteProduct = makeDeleteProduct({deleteDataProduct})
const createProduct = makeCreateProduct({createDataProduct})
const updateProduct = makeUpdateProduct({updateDataProduct})
const insertProductFromElevenia = makeInsertProductFromElevenia({insertDataProductFromElevenia})

const createTransaction = makeCreateTransaction({createDataTransaction})
const updateTransaction = makeUpdateTransaction({updateDataTransaction})
const deleteTransaction = makeDeleteTransaction({deleteDataTransaction})
const getTransactionList = makeGetTransactionList({getDataTransactionList})
const getTransactionDetail = makeGetTransactionDetail({getDataTransactionDetail})
const cancelTransaction = makeCancelTransaction({cancelDataTransaction})
const acceptTransaction = makeAcceptTransaction({acceptDataTransaction})

const ecommerceController =  Object.freeze({
   getProductList,
   getProductDetail,
   deleteProduct,
   createProduct,
   updateProduct,
   insertProductFromElevenia, 

   createTransaction, 
   updateTransaction, 
   deleteTransaction, 
   getTransactionList, 
   getTransactionDetail,
   cancelTransaction, 
   acceptTransaction
})
  
export default  ecommerceController
export {
    getProductList,
    getProductDetail,
    deleteProduct,
    createProduct,
    updateProduct,
    insertProductFromElevenia, 
    
    createTransaction, 
    updateTransaction, 
    deleteTransaction, 
    getTransactionList, 
    getTransactionDetail, 
    cancelTransaction, 
    acceptTransaction
}  