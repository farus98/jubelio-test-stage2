import camelcaseKeys from 'camelcase-keys';
export default function makeAcceptTransaction({acceptDataTransaction}) {
  return async function acceptTransaction(httpRequest) {
    try {
      
      const posted = await acceptDataTransaction(camelcaseKeys(httpRequest))
      
      return {
        status : true,
        ...posted
      }

    } catch (err:any) {
      //TODO: Error logging
      console.log(err)
      
      return {
        statusCode: 404,
        body: {
          status : false,
          responseCode : 404,
          message: err.message
        }
      }
    }
  }    
}