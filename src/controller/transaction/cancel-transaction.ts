import camelcaseKeys from 'camelcase-keys';
export default function makeCancelTransaction({cancelDataTransaction}) {
  return async function cancelTransaction(httpRequest) {
    try {
      
      const posted = await cancelDataTransaction(camelcaseKeys(httpRequest))
      
      return {
        status : true,
        ...posted
      }

    } catch (err:any) {
      //TODO: Error logging
      console.log(err)
      
      return {
        statusCode: 404,
        body: {
          status : false,
          responseCode : 404,
          message: err.message
        }
      }
    }
  }    
}