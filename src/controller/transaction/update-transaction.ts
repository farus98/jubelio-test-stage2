
import camelcaseKeys from 'camelcase-keys';
export default function makeUpdateTransaction({updateDataTransaction}) {
  return async function updateTransaction(httpRequest) {
    try {
      
      const posted = await updateDataTransaction(camelcaseKeys(httpRequest))
      
      return {
        status : true,
        ...posted
      }

    } catch (err:any) {
      //TODO: Error logging
      console.log(err)
      
      return {
        statusCode: 404,
        body: {
          status : false,
          responseCode : 404,
          message: err.message
        }
      }
    }
  }    
}