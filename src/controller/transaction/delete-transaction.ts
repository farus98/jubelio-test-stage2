export default function makeDeleteTransaction({deleteDataTransaction}) {
  return async function deleteTransaction(httpRequest) {
    try {
    
      const posted = await deleteDataTransaction(httpRequest)
      
      return {
        status : true,
        ...posted
      }

    } catch (err:any) {
      //TODO: Error logging
      console.log(err)
      
      return {
        statusCode: 404,
        body: {
          status : false,
          responseCode : 404,
          message: err.message
        }
      }
    }
  }          
}  