export default function makeGetTransactionDetail({getDataTransactionDetail}) {
  return async function getTransactionDetail(httpRequest) {
    try {
    
      const posted = await getDataTransactionDetail(httpRequest)
      
      return {
        status : true,
        ...posted
      }

    } catch (err:any) {
      //TODO: Error logging
      console.log(err)
      
      return {
        statusCode: 404,
        body: {
          status : false,
          responseCode : 404,
          message: err.message
        }
      }
    }
  }      
}  