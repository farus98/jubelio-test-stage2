export default function makeGetTransactionList({getDataTransactionList}) {
  return async function getTransactionList(httpRequest) {
    try {
    
      const posted = await getDataTransactionList(httpRequest)
      
      return {
        status : true,
        ...posted
      }

    } catch (err:any) {
      //TODO: Error logging
      console.log(err)
      
      return {
        statusCode: 404,
        body: {
          status : false,
          responseCode : 404,
          message: err.message
        }
      }
    }
  }    
}