export default function makeGetProductDetail({getDataProductDetail}) {
  return async function getProductDetail(httpRequest) {
    try {
    
      const posted = await getDataProductDetail(httpRequest)
      
      return {
        status : true,
        ...posted
      }

    } catch (err:any) {
      //TODO: Error logging
      console.log(err)
      
      return {
        statusCode: 404,
        body: {
          status : false,
          responseCode : 404,
          message: err.message
        }
      }
    }
  }  
}  