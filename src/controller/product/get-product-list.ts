export default function makeGetProductList({getDataProductList}) {
  return async function getProductList(httpRequest) {
    try {
    
      const posted = await getDataProductList(httpRequest)
      
      return {
        status : true,
        ...posted
      }

    } catch (err:any) {
      //TODO: Error logging
      console.log(err)
      
      return {
        statusCode: 404,
        body: {
          status : false,
          responseCode : 404,
          message: err.message
        }
      }
    }
  }
}
