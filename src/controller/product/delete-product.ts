export default function makeDeleteProduct({deleteDataProduct}) {
  return async function deleteProduct(httpRequest) {
    try {
    
      const posted = await deleteDataProduct(httpRequest)
      
      return {
        status : true,
        ...posted
      }

    } catch (err:any) {
      //TODO: Error logging
      console.log(err)
      
      return {
        statusCode: 404,
        body: {
          status : false,
          responseCode : 404,
          message: err.message
        }
      }
    }
  }      
}  