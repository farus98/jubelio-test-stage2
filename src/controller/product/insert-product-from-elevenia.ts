export default function makeInsertProductFromElevenia({insertDataProductFromElevenia}) {
  return async function insertProductFromElevenia(httpRequest) {
    try {
      
      const posted = await insertDataProductFromElevenia(httpRequest)
      
      return {
        status : true,
        responseCode: 200, 
        data: posted
      }

    } catch (err:any) {
      //TODO: Error logging
      console.log(err)
      
      return {
        statusCode: 404,
        body: {
          status : false,
          responseCode : 404,
          message: err.message
        }
      }
    }
  }    
}