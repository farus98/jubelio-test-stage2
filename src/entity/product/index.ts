import buildMakeProduct from './product'
import moment from 'moment'

moment.locale('id')
const makeProduct = buildMakeProduct(moment)

export default makeProduct
