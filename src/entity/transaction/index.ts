import buildMakeTransaction from './transaction'
import moment from 'moment'

moment.locale('id')
const makeTransaction = buildMakeTransaction(moment)

export default makeTransaction
