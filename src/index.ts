import Hapi from '@hapi/hapi'

import {
  getProductList,
  getProductDetail,
  deleteProduct,
  createProduct,
  updateProduct,
  insertProductFromElevenia, 

  createTransaction, 
  updateTransaction, 
  deleteTransaction, 
  getTransactionList, 
  getTransactionDetail, 
  cancelTransaction, 
  acceptTransaction
} from "./controller"

const init = async () => {

  const server = Hapi.server({
      port: 3000,
      host: 'localhost'
  });

  server.route({
    method: 'GET',
    path:'/product/list',
    handler: async (req,res) => {
      console.log('product list ');
      
      const result = await getProductList(req.query)
      return result
    }
  });

  server.route({
    method: 'GET',
    path:'/product/detail',
    handler: async (req,res) => {
      const result = await getProductDetail(req.query)
      return result
    }
  });

  server.route({
    method: 'DELETE',
    path:'/product',
    handler: async (req,res) => {
      const result = await deleteProduct(req.query)
      return result
    }
  });

  server.route({
    method: 'POST',
    path:'/product',
    handler: async (req,res) => {
      const result = await createProduct(req.payload)
      return result
    }
  });

  server.route({
    method: 'PUT',
    path:'/product',
    handler: async (req,res) => {
      const result = await updateProduct(req.payload)
      return result
    }
  });

  server.route({
    method: 'GET',
    path:'/product/elevenia',
    handler: async (req,res) => {
      const result = await insertProductFromElevenia(req.query)
      return result
    }
  });

  server.route({
    method: 'GET',
    path:'/transaction/list',
    handler: async (req,res) => {
      const result = await getTransactionList(req.query)
      return result
    }
  });

  server.route({
    method: 'GET',
    path:'/transaction/detail',
    handler: async (req,res) => {
      const result = await getTransactionDetail(req.query)
      return result
    }
  });

  server.route({
    method: 'DELETE',
    path:'/transaction',
    handler: async (req,res) => {
      const result = await deleteTransaction(req.query)
      return result
    }
  });

  server.route({
    method: 'POST',
    path:'/transaction',
    handler: async (req,res) => {
      const result = await createTransaction(req.payload)
      return result
    }
  });

  server.route({
    method: 'PUT',
    path:'/transaction',
    handler: async (req,res) => {
      const result = await updateTransaction(req.payload)
      return result
    }
  });

  server.route({
    method: 'POST',
    path:'/transaction/cancel',
    handler: async (req,res) => {
      const result = await cancelTransaction(req.payload)
      return result
    }
  });

  server.route({
    method: 'POST',
    path:'/transaction/accept',
    handler: async (req,res) => {
      const result = await acceptTransaction(req.payload)
      return result
    }
  });

  await server.start();
  console.log('Server running on port 3000');

};

init();