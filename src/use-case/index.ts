import { productDb, transactionDb } from "../data-access"
import makeProduct from "../entity/product"
import makeTransaction from "../entity/transaction"
import { requestElevenia } from "../middleware"

import makeGetProductList from "./product/get-product-list"
import makeGetProductDetail from "./product/get-product-detail"
import makeDeleteProduct from "./product/delete-product"
import makeCreateProduct from "./product/create-product"
import makeUpdateProduct from "./product/update-product"
import makeInsertProductFromElevenia from "./product/insert-product-from-elevenia"

import makeCreateTransaction from "./transaction/create-transaction"
import makeUpdateTransaction from "./transaction/update-transaction"
import makeGetTransactionList from "./transaction/get-transaction-list"
import makeGetTransactionDetail from "./transaction/get-transaction-detail"
import makeDeleteTransaction from "./transaction/delete-transaction"
import makeCancelTransaction from "./transaction/cancel-transaction"
import makeAcceptTransaction from "./transaction/accept-transaction"

const getDataProductList = makeGetProductList({productDb})
const getDataProductDetail = makeGetProductDetail({productDb})
const deleteDataProduct = makeDeleteProduct({productDb, transactionDb})
const createDataProduct = makeCreateProduct({productDb,makeProduct})
const updateDataProduct = makeUpdateProduct({productDb,makeProduct})
const insertDataProductFromElevenia = makeInsertProductFromElevenia({productDb,makeProduct,requestElevenia})

const createDataTransaction = makeCreateTransaction({transactionDb, productDb, makeTransaction})
const updateDataTransaction = makeUpdateTransaction({transactionDb,productDb,makeTransaction})
const getDataTransactionList = makeGetTransactionList({transactionDb})
const getDataTransactionDetail = makeGetTransactionDetail({transactionDb})
const deleteDataTransaction = makeDeleteTransaction({transactionDb})
const cancelDataTransaction = makeCancelTransaction({transactionDb, makeTransaction})
const acceptDataTransaction = makeAcceptTransaction({transactionDb, makeTransaction})

const ecommerceService = Object.freeze({
    getDataProductList,
    getDataProductDetail,
    deleteDataProduct, 
    createDataProduct,
    updateDataProduct,
    insertDataProductFromElevenia, 
    
    createDataTransaction, 
    updateDataTransaction, 
    getDataTransactionList, 
    getDataTransactionDetail, 
    deleteDataTransaction, 
    cancelDataTransaction, 
    acceptDataTransaction
})

export default ecommerceService
export {
    getDataProductList,
    getDataProductDetail,
    deleteDataProduct,
    createDataProduct,
    updateDataProduct,
    insertDataProductFromElevenia, 
    
    createDataTransaction, 
    updateDataTransaction, 
    getDataTransactionList, 
    getDataTransactionDetail, 
    deleteDataTransaction, 
    cancelDataTransaction, 
    acceptDataTransaction
}