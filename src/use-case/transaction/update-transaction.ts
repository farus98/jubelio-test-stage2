export default function makeUpdateTransaction({transactionDb, productDb, makeTransaction}) {
  return async function updateTransaction(body:any) {
    try {
      console.log('usecase update transaction');
      
      if(body.qty <= 0){
        throw new Error("Qty Must Be More Than 0")
      }

      const getQty = await transactionDb.getQty(body)
      console.log('getQty', getQty);
      

      const getPrice = await productDb.getPriceProduct({sku: body.sku})

      const totalPrice = getPrice*body.qty

      const transaction = await makeTransaction({...body, amount: totalPrice})

      const updateTransaction = await transactionDb.updateTransaction(transaction)

      let result 

      if(updateTransaction > 0){
        if(transaction.getQty() < getQty){
          //increase
          const increaseStock = await transactionDb.increaseStock({sku: transaction.getSku(), qty: transaction.getQty()})
          console.log('increase stock', increaseStock);
        }else{
          //decrease
          const decreaseStock = await transactionDb.decreaseStock({sku: transaction.getSku(), qty: transaction.getQty()})
          console.log('decrease stock', decreaseStock);
        }
        result = {responseCode: 201, information: "Your Transaction Has Been Successfully Updated"}
      }else{
        result = {responseCode: 400, information: "Your Transaction Failed Updated"}
      }

       return result
      
  
    } catch (error:any) {
      throw new Error(error);
    }
  
  }        
}  