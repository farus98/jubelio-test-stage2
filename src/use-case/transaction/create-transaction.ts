export default function makeCreateTransaction({transactionDb, productDb, makeTransaction}) {
    return async function createTransaction(body:any) {
      try {
      
        const checkStock = await productDb.checkStock({sku: body.sku})

        let result

        if(checkStock.stock > 0){
          if(body.qty > checkStock.stock){
            throw new Error("Transaction Failed Due To Insufficient Stock Quantity")
          }

          const getPrice = await productDb.getPriceProduct({sku: body.sku})
          const totalPrice = getPrice*body.qty

          const transaction = await makeTransaction({...body, amount: totalPrice})

          const insertTransaction = await transactionDb.createTransaction(transaction)

          if(insertTransaction > 0){

            const decreaseStock = await transactionDb.decreaseStock({sku: transaction.getSku(), qty: transaction.getQty()})
            console.log('decrease stock', decreaseStock);

            result = {responseCode: 201, information: "Your Transaction is Successfully Recorded, Your Purchase Amount is "+totalPrice}
          }else{
            result = {responseCode: 400, information: "Your Transaction Failed Recorded"}
          }

        }else{
          throw new Error("Stock Product Less Then 0 Can't Make Transactions")
        }

        return result
    
      } catch (error:any) {
        throw new Error(error);
      }
    
    }    
  }  