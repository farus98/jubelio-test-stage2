export default function makeAcceptTransaction({transactionDb, makeTransaction}) {
    return async function acceptTransaction(body:any) {
      try {

        const order = await makeTransaction(body)

        const acceptOrder = await transactionDb.acceptOrder(order)

        let result 

        if(acceptOrder > 0){
          result = {responseCode: 201, information: "order with sku "+body.sku+" and user "+body.user+" has been accepted "}
        }else{
          result = {responseCode: 400, information: "order with sku "+body.sku+" and user "+body.user+" failed to accepted "}
        }
  
        return result
        
    
      } catch (error:any) {
        throw new Error(error);
      }
    
    }        
  }  