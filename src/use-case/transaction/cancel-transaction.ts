export default function makeCancelTransaction({transactionDb, makeTransaction}) {
    return async function cancelTransaction(body:any) {
      try {

        const order = await makeTransaction(body)

        const cancelOrder = await transactionDb.cancelOrder(order)

        let result 

        if(cancelOrder > 0){
          result = {responseCode: 201, information: "order with sku "+body.sku+" and user "+body.user+" has been canceled "}
        }else{
          result = {responseCode: 400, information: "order with sku "+body.sku+" and user "+body.user+" failed to canceled "}
        }
  
        return result
        
    
      } catch (error:any) {
        throw new Error(error);
      }
    
    }        
  }  