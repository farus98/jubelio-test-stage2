export default function makeGetTransactionDetail({transactionDb}) {
  return async function getTransactionDetail(body:any) {
    try {
    
      const getData =  await transactionDb.showTransactionDetail(body);
      
      let result 

      if(getData.length > 0){
        result = {responseCode: 200, information: "Data Found", data: getData[0]}
      }else{
        result = {responseCode: 204, information: "Data Not Found", data: {}}
      }

      return Object.freeze(result);

    } catch (error:any) {
      throw new Error(error);
    }
  
  }        
}  