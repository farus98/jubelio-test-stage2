export default function makeDeleteTransaction({transactionDb}) {
  return async function deleteTransaction(body:any) {
    try {
      
      const deleteData =  await transactionDb.deleteTransactionData(body);
      
      let result
      
      if(deleteData > 0){
        result = {responseCode: 200, information: 'Deleted Data Success'}
      }else{
        result = {responseCode: 204, information: 'Deleted Data Does Not Exist'}
      }

      return Object.freeze(result);
      
    } catch (error:any) {
      throw new Error(error);
    }
  
  }        
}  