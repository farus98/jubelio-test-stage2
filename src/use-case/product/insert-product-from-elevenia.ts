const xml2js = require('xml2js');

export default function makeInsertProductFromElevenia({productDb, makeProduct, requestElevenia}) {
  return async function insertProductFromElevenia(body:any) {
    try {
    
      const getDataElevenia = await requestElevenia.getDataProductElevenia()

      let jsonDataElevenia

      xml2js.parseString(getDataElevenia, (err, result) => {
        if(err) {
          throw err;
        }

        jsonDataElevenia = result.Products.product
      });
      
      let insertData = await Promise.all(
        jsonDataElevenia.map(async data =>{
          const entity = {
            sku: data.sellerPrdCd[0],
            name: data.prdNm[0],
            image: data.imageKindChk[0],
            price: data.selPrc[0],
            stock: data.ProductOptionDetails[0].stckQty[0],
            description: '',
            user: 'system',
          }
          
          const product = await makeProduct(entity)

          const checkSkuAvaliable = await productDb.checkAvailableSku(product)

          if(checkSkuAvaliable == true){

            return {responseCode: 400, information: "Product SKU "+entity.sku+" already exists, cannot be duplicated"}
          
          }else{

            const checkAvailableNameProduct = productDb.checkNameProduct({...product, identity: 'create'})
          
            if(checkAvailableNameProduct == true){
          
              return {responseCode: 400, information: "Product Name with SKU "+entity.sku+" already exists, cannot be duplicated"}
          
            }else{

              const insertData = await productDb.createProduct(product)

              if(insertData > 0){
                return {information: "Data SKU "+product.getSku()+" Success Inserted"}
              }else{
                return {information: "Data SKU "+product.getSku()+" Failed Inserted"}
              }
              
            }
  
          }
        })
      )

      return insertData

    } catch (error:any) {
      throw new Error(error);
    }
  
  }        
}  