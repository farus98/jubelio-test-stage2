export default function makeDeleteProduct({productDb, transactionDb}) {
    return async function deleteProduct(body:any) {
      try {
        
        if(!body.sku && !body.id){
          throw new Error("not paramater identify")
        }

        let sku 
        if(!body.sku){
          const getSku = await productDb.getSku(body)
          sku = getSku
        }else{
          sku = body.sku
        }

        const deleteData =  await productDb.deleteProductData(body);
        
        let result
        
        if(deleteData > 0){
          const deleteDataTransaction = await transactionDb.deleteTransactionData({sku: sku})
          console.log('Information Delete Transaction : ', deleteDataTransaction);
          
          result = {responseCode: 200, information: 'Deleted Data Success'}
        }else{
          result = {responseCode: 204, information: 'Deleted Data Does Not Exist'}
        }

        return Object.freeze(result);
        
      } catch (error:any) {
        throw new Error(error);
      }
    
    }    
  }  