export default function makeCreateProduct({productDb, makeProduct}) {
  return async function createProduct(body:any) {
    try {
    
      const product = await makeProduct(body)

      const checkSkuAvaliable = await productDb.checkAvailableSku(product)

      if(checkSkuAvaliable == true){
        throw new Error("SKU already exists, cannot be duplicated")
      }

      const checkAvailableNameProduct = productDb.checkNameProduct({...product, identity: 'create'})
      
      if(checkAvailableNameProduct == true){
        throw new Error("Name already exists, cannot be duplicated")
      }

      const insertData = await productDb.createProduct(product)

      let result 

      if(insertData > 0){
        result = {responseCode: 201, information: "Data SKU "+product.getSku()+" Success Inserted"}
      }else{
        result = {responseCode: 400, information: "Data SKU "+product.getSku()+" Failed Inserted"}
      }

      return result
  
    } catch (error:any) {
      throw new Error(error);
    }
  
  }    
}  