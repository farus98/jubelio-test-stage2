export default function makeGetProductDetail({productDb}) {
  return async function getProductDetail(body:any) {
    try {
    
      const getData =  await productDb.showProductDetail(body);
      // console.log('data', getData);
      
      let result 

      if(getData.length > 0){
        result = {responseCode: 200, information: "Data Found", data: getData[0]}
      }else{
        result = {responseCode: 204, information: "Data Not Found", data: {}}
      }

      return Object.freeze(result);

    } catch (error:any) {
      throw new Error(error);
    }
  
  }    
}  