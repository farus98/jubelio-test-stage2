export default function makeGetProductList({productDb}) {
  return async function getProductList(body:any) {
    try {
    
      if(body.page < 1){
        throw new Error("page not less then one")
      }
  
      const getData =  await productDb.showProductList(body);
      
      let result 

      if(getData.length > 0){
        result = {responseCode: 200, information: "Data Found", data: getData, totalData: getData.length}
      }else{
        result = {responseCode: 204, information: "Data Not Found", data: [], totalData: 0}
      }

      return Object.freeze(result);

    } catch (error:any) {
      throw new Error(error);
    }
  
  }
}  