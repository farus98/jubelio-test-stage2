export default function makeUpdateProduct({productDb, makeProduct}) {
  return async function updateProduct(body:any) {
    try {
    
      const product = await makeProduct(body)

      const checkAvailableNameProduct = await productDb.checkNameProduct({...product, identity: 'update'})
      console.log('check name', checkAvailableNameProduct);
      
      if(checkAvailableNameProduct == true){
        throw new Error("Name already exists, cannot be duplicated")
      }

      const updateData = await productDb.updateProduct(product)

      let result 

      if(updateData > 0){
        result = {responseCode: 201, information: "Data SKU "+product.getSku()+" Success Updated"}
      }else{
        result = {responseCode: 400, information: "Data SKU "+product.getSku()+" Failed Updated"}
      }

      return result
  
    } catch (error:any) {
      throw new Error(error);
    }
  
  }        
}  