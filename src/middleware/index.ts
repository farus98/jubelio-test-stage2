import makeRequestElevenia from "./elevenia"

const requestElevenia = makeRequestElevenia()

const jubelioMiddleware = Object.freeze({
    requestElevenia
})

export default jubelioMiddleware

export {
    requestElevenia
}
