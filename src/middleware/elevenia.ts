import axios from 'axios'

export default function makeRequestElevenia() {
  return Object.freeze({
    getDataProductElevenia
  })

  async function getDataProductElevenia(reqsms){
    return new Promise(function(resolve, reject) {
      
      axios ({
        method: 'GET',
        url: "http://api.elevenia.co.id/rest/prodservices/product/listing?page=1",
        headers: {
          'Content-Type': 'application/xml',
          openapikey: '721407f393e84a28593374cc2b347a98'
        }
      })
      .then(result =>{
        // console.log('data',result)
        resolve(result.data)
      })
      .catch(err =>{
        // console.log('err',err)
        reject(new Error(err))
      })
    })
  }



}
